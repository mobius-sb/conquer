# Project

This is a prototype server for Conquer Online using C# 9, .NET 5, and ASP.NET Core.

Inspired by [Spirited/Comet](https://gitlab.com/spirited/comet).

# Getting started

* Install [.NET 5](https://dotnet.microsoft.com/download/dotnet/5.0).
* The default settings inside the `appsettings.json` file use [LocalDB](https://docs.microsoft.com/en-us/sql/database-engine/configure-windows/sql-server-express-localdb?view=sql-server-ver15), which usually comes with Visual Studio.
* You may need to install [EF Core tools](https://docs.microsoft.com/en-us/ef/core/cli/dotnet) to run `dotnet ef` commands.
* The server name in your `Server.dat` file must be `Classic`. You can change this in the `appsettings.json` file.

Clone the project and run the following commands from the same directory as the Conquer.csproj file.

```
dotnet ef database update
dotnet run
```

The project comes with migrations that create the database and seed it with sample data. There are two test accounts.

Account 1:
```
Test1
Password1
```

Account 2 (banned):
```
Test2
Password2
```
