﻿using Conquer.Cryptography;
using Microsoft.AspNetCore.Connections;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO.Pipelines;
using System.Threading.Tasks;

namespace Conquer.Middleware
{
    public static class ListenOptionsExtensions
    {
        public static ListenOptions UseConnectionCipher<T>(this ListenOptions listenOptions) where T : IConnectionCipher
        {
            listenOptions.Use(next => new ConnectionCipherMiddleware<T>(next, listenOptions.ApplicationServices).OnConnectionAsync);
            return listenOptions;
        }
    }

    public class ConnectionCipherMiddleware<T> where T : IConnectionCipher
    {
        private readonly ConnectionDelegate _next;
        private readonly IServiceProvider _serviceProvider;

        public ConnectionCipherMiddleware(ConnectionDelegate next, IServiceProvider serviceProvider)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
        }

        public async Task OnConnectionAsync(ConnectionContext connection)
        {
            var oldTransport = connection.Transport;
            try
            {
                var cipher = ActivatorUtilities.GetServiceOrCreateInstance<T>(_serviceProvider);
                var logger = _serviceProvider.GetRequiredService<ILogger<ConnectionCipherDuplexPipe>>();
                await using var loggingDuplexPipe = new ConnectionCipherDuplexPipe(connection.Transport, cipher, logger, PipeOptions.Default, PipeOptions.Default);
                connection.Transport = loggingDuplexPipe;
                connection.Features.Set<IConnectionCipher>(cipher);
                await _next(connection);
            }
            finally
            {
                connection.Transport = oldTransport;
            }
        }
    }

    public class ConnectionCipherDuplexPipe : IDuplexPipe, IAsyncDisposable
    {
        private readonly IDuplexPipe _transport;
        private readonly IConnectionCipher _cipher;
        private readonly ILogger _logger;
        private readonly Pipe _input, _output;
        private readonly Task _decryptTask, _encryptTask;

        public ConnectionCipherDuplexPipe(IDuplexPipe transport, IConnectionCipher cipher, ILogger<ConnectionCipherDuplexPipe> logger, PipeOptions inputOptions, PipeOptions outputOptions)
        {
            _transport = transport;
            _cipher = cipher;
            _logger = logger;
            _input = new(inputOptions);
            _output = new(outputOptions);
            _decryptTask = DecryptAsync();
            _encryptTask = EncryptAsync();
        }

        public PipeReader Input => _input.Reader;
        public PipeWriter Output => _output.Writer;

        private async Task DecryptAsync()
        {
            try
            {
                while (true)
                {
                    var readResult = await _transport.Input.ReadAsync();
                    var buffer = readResult.Buffer;

                    foreach (var readOnlyMemory in buffer)
                    {
                        var memory = _input.Writer.GetMemory(readOnlyMemory.Length);
                        _cipher.Decrypt(readOnlyMemory.Span, memory.Span);
                        _input.Writer.Advance(readOnlyMemory.Length);
                    }

                    _transport.Input.AdvanceTo(buffer.End);

                    var flushResult = await _input.Writer.FlushAsync();

                    if (flushResult.IsCanceled || flushResult.IsCompleted)
                    {
                        break;
                    }

                    if (readResult.IsCanceled || readResult.IsCompleted)
                    {
                        break;
                    }
                }
            }
            catch (ConnectionResetException)
            {
                // Don't let connection reset exceptions out
            }
            catch (ConnectionAbortedException)
            {
                // Don't let connection aborted exceptions out
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unexpected exception in {Method}.", nameof(DecryptAsync));
            }

            await _transport.Input.CompleteAsync();
            await _input.Writer.CompleteAsync();
        }

        private async Task EncryptAsync()
        {
            try
            {
                while (true)
                {
                    var readResult = await _output.Reader.ReadAsync();
                    var buffer = readResult.Buffer;

                    foreach (var readOnlyMemory in buffer)
                    {
                        var memory = _transport.Output.GetMemory(readOnlyMemory.Length);
                        _cipher.Encrypt(readOnlyMemory.Span, memory.Span);
                        _transport.Output.Advance(readOnlyMemory.Length);
                    }

                    _output.Reader.AdvanceTo(buffer.End);

                    var flushResult = await _transport.Output.FlushAsync();

                    if (flushResult.IsCanceled || flushResult.IsCompleted)
                    {
                        break;
                    }

                    if (readResult.IsCanceled || readResult.IsCompleted)
                    {
                        break;
                    }
                }
            }
            catch (ConnectionResetException)
            {
                // Don't let connection reset exceptions out
            }
            catch (ConnectionAbortedException)
            {
                // Don't let connection aborted exceptions out
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unexpected exception in {Method}.", nameof(EncryptAsync));
            }

            await _output.Reader.CompleteAsync();
            await _transport.Output.CompleteAsync();
        }

        public async ValueTask DisposeAsync()
        {
            await Input.CompleteAsync();
            await Output.CompleteAsync();
            await _decryptTask;
            await _encryptTask;
        }
    }
}
