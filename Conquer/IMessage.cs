﻿using System;
using System.Threading.Tasks;

namespace Conquer
{
    public interface IMessage<T>
    {
        ushort Size { get; }
        MessageType Type { get; }

        Task HandleAsync(T context) => Task.CompletedTask;
        void Read(ReadOnlySpan<byte> buffer);
        void Write(Span<byte> buffer);
    }
}
