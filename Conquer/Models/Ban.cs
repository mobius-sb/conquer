﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Conquer.Models
{
    public class Ban
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public IdentityUser<int> User { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Expires { get; set; }
        public string Reason { get; set; }
    }
}
