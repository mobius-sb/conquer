﻿namespace Conquer.Models
{
    public class Server
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string IpAddress { get; set; }
        public int Port { get; set; }
    }
}
