﻿namespace Conquer.Models
{
    public enum PkMode
    {
        Pk,
        Peace,
        Team,
        Capture
    }

    public enum Profession
    {
        InternTrojan = 10,
        Trojan,
        VeteranTrojan,
        TigerTrojan,
        DragonTrojan,
        TrojanMaster,

        InternWarrior = 20,
        Warrior,
        BrassWarrior,
        SilverWarrior,
        GoldWarrior,
        WarriorMaster,

        InternArcher = 40,
        Archer,
        EagleArcher,
        TigerArcher,
        DragonArcher,
        ArcherMaster,

        InternTaoist = 100,
        Taoist,

        WaterTaoist = 132,
        WaterWizard,
        WaterMaster,
        WaterSaint,

        FireTaoist = 142,
        FireWizard,
        FireMaster,
        FireSaint
    }

    public class Player
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public int Model { get; set; }
        public short Avatar { get; set; }
        public short Hair { get; set; }
        public int Money { get; set; }
        public long Experience { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Vitality { get; set; }
        public int Mana { get; set; }
        public int AttributePoints { get; set; }
        public int Health { get; set; }
        public int Magic { get; set; }
        public PkMode PkMode { get; set; }
        public short PkPoints { get; set; }
        public byte Level { get; set; }
        public Profession Profession { get; set; }
        public int MapId { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public byte Rebirths { get; set; }
        public int? SpouseId { get; set; }
        public Player Spouse { get; set; }
    }
}
