﻿using System;
using System.Buffers.Binary;
using System.Numerics;

namespace Conquer.Cryptography
{
    public class Rc5PasswordCipher : IPasswordCipher
    {
        private static readonly uint[] _key = new uint[]
        {
            0xebe854bc, 0xb04998f7, 0xfffaa88c, 0x96e854bb, 0xa9915556, 0x48e44110,
            0x9f32308f, 0x27f41d3e, 0xcf4f3523, 0xeac3c6b4, 0xe9ea5e03, 0xe5974bba,
            0x334d7692, 0x2c6bcf2e, 0xdc53b74, 0x995c92a6, 0x7e4f6d77, 0x1eb2b79f,
            0x1d348d89, 0xed641354, 0x15e04a9d, 0x488da159, 0x647817d3, 0x8ca0bc20,
            0x9264f7fe, 0x91e78c6c, 0x5c9a07fb, 0xabd4dcce, 0x6416f98d, 0x6642ab5b
         };

        public void Encrypt(ReadOnlySpan<byte> source, Span<byte> destination)
        {
            for (int i = 1; i >= 0; i--)
            {
                uint temp1 = _key[5] + BinaryPrimitives.ReadUInt32LittleEndian(source.Slice(((i * 2) + 1) * 4, 4));
                uint temp2 = _key[4] + BinaryPrimitives.ReadUInt32LittleEndian(source.Slice(i * 2 * 4, 4));

                for (int j = 0; j < 12; j++)
                {
                    temp2 = BitOperations.RotateLeft(temp1 ^ temp2, (int)temp1) + _key[(j * 2) + 6];
                    temp1 = BitOperations.RotateLeft(temp1 ^ temp2, (int)temp2) + _key[(j * 2) + 7];
                }

                BinaryPrimitives.WriteUInt32LittleEndian(destination.Slice(((i * 2) + 1) * 4, 4), temp1);
                BinaryPrimitives.WriteUInt32LittleEndian(destination.Slice(i * 2 * 4, 4), temp2);
            }
        }

        public void Decrypt(ReadOnlySpan<byte> source, Span<byte> destination)
        {
            for (int i = 1; i >= 0; i--)
            {
                uint temp1 = BinaryPrimitives.ReadUInt32LittleEndian(source.Slice(((i * 2) + 1) * 4, 4));
                uint temp2 = BinaryPrimitives.ReadUInt32LittleEndian(source.Slice(i * 2 * 4, 4));

                for (int j = 11; j >= 0; j--)
                {
                    temp1 = BitOperations.RotateRight(temp1 - _key[(j * 2) + 7], (int)temp2) ^ temp2;
                    temp2 = BitOperations.RotateRight(temp2 - _key[(j * 2) + 6], (int)temp1) ^ temp1;
                }

                BinaryPrimitives.WriteUInt32LittleEndian(destination.Slice(((i * 2) + 1) * 4, 4), temp1 - _key[5]);
                BinaryPrimitives.WriteUInt32LittleEndian(destination.Slice(i * 2 * 4, 4), temp2 - _key[4]);
            }
        }
    }
}
