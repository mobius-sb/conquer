﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Conquer.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Servers",
                columns: new[] { "Id", "Name", "IpAddress", "Port" },
                values: new object[] { 1, "Classic", "127.0.0.1", 5816 });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "UserName", "NormalizedUserName", "EmailConfirmed", "PasswordHash", "SecurityStamp", "ConcurrencyStamp", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEnabled", "AccessFailedCount" },
                values: new object[,]
                {
                    { 1, "Test1", "TEST1", false, "AQAAAAEAACcQAAAAEEFXrELQwznH5/4c8gJp+SpvWCSi7pSPXIZHjh3xeflOl/VsC3mVFDgrCXQHq67nQw==", "7V2BD6OLEE5MOYOWTXC56PBEJUTLCAWY", "ab1e9000-95d0-4eb6-9292-54927056821f", false, false, true, 0 },
                    { 2, "Test2", "TEST2", false, "AQAAAAEAACcQAAAAECesqxEPMbJk93gQ4l8Lj3fHgDDLf3j2zcVxhznzvnIrBj4TMvzBztCjiIxpWEhipQ==", "FWIY4U6ROBOYQIRZRUOFI36RQZSB5WSZ", "4bcbd2cb-2df2-41bb-9820-57a88c9d8b94", false, false, true, 0 }
                });

            migrationBuilder.InsertData(
                table: "Bans",
                columns: new[] { "Id", "UserId", "Created", "Reason" },
                values: new object[] { 1, 2, DateTime.Now, "Testing" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Servers",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "Bans",
                keyColumn: "Id",
                keyValue: 1);
        }

    }
}
