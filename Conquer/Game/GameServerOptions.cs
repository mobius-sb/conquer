﻿using System;

namespace Conquer.Game
{
    public class GameServerOptions
    {
        public string Name { get; set; }
        public int Port { get; set; }
        public TimeSpan ClientTimeoutInterval { get; set; }
    }
}
