﻿using Microsoft.AspNetCore.Connections;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace Conquer.Game
{
    public class GameServer : ConnectionHandler
    {
        private readonly GameServerOptions _options;
        private readonly IServiceProvider _serviceProvider;
        private readonly ILoggerFactory _loggerFactory;
        private readonly ILogger _logger;

        public GameServer(IOptions<GameServerOptions> options, IServiceProvider serviceProvider, ILoggerFactory loggerFactory)
        {
            _options = options.Value;
            _serviceProvider = serviceProvider;
            _loggerFactory = loggerFactory;
            _logger = loggerFactory.CreateLogger<GameServer>();
        }

        public string Name => _options.Name;
        public ConcurrentDictionary<int, GameClient> Clients { get; } = new();

        public override async Task OnConnectedAsync(ConnectionContext connection)
        {
            var client = ActivatorUtilities.CreateInstance<GameClient>(_serviceProvider, connection);
            
            _logger.LogInformation("{RemoteEndPoint} connected.", client.RemoteEndPoint);

            try
            {
                using var source = new CancellationTokenSource();
                while (true)
                {
                    source.CancelAfter(_options.ClientTimeoutInterval);
                    var message = await client.ReadAsync(source.Token);

                    if (message is null)
                    {
                        break;
                    }

                    using var scope = _serviceProvider.CreateScope();
                    var context = ActivatorUtilities.CreateInstance<GameContext>(scope.ServiceProvider, client, _loggerFactory.CreateLogger(message.GetType()));
                    await message.HandleAsync(context);
                }
            }
            catch (OperationCanceledException)
            {
                // Don't let operation canceled exceptions out
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unexpected exception in {Method}.", nameof(OnConnectedAsync));
            }

            if (client.Player is not null)
            {
                Clients.TryRemove(client.Player.Id, out _);
            }

            _logger.LogInformation("{RemoteEndPoint} disconnected.", client.RemoteEndPoint);
        }
    }
}
