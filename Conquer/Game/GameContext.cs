﻿using Conquer.Data;
using Conquer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Security.Claims;
using System.Security.Cryptography;

namespace Conquer.Game
{
    public class GameContext
    {
        public GameContext(
            ConquerDbContext database,
            GameServer server,
            GameClient client,
            IServiceProvider serviceProvider,
            ILogger logger,
            RandomNumberGenerator randomNumberGenerator,
            IDistributedCache cache,
            UserManager<IdentityUser<int>> userManager,
            IUserClaimsPrincipalFactory<IdentityUser<int>> userClaimsPrincipalFactory)
        {
            Database = database;
            Server = server;
            Client = client;
            ServiceProvider = serviceProvider;
            Logger = logger;
            RandomNumberGenerator = randomNumberGenerator;
            Cache = cache;
            UserManager = userManager;
            UserClaimsPrincipalFactory = userClaimsPrincipalFactory;
        }

        public ConquerDbContext Database { get; }
        public GameServer Server { get; }
        public ConcurrentDictionary<int, GameClient> Clients => Server.Clients;
        public GameClient Client { get; }
        public ClaimsPrincipal User { get => Client.User; set => Client.User = value; }
        public Player Player { get => Client.Player; set => Client.Player = value; }
        public IServiceProvider ServiceProvider { get; }
        public ILogger Logger { get; }
        public RandomNumberGenerator RandomNumberGenerator { get; }
        public IDistributedCache Cache { get; }
        public UserManager<IdentityUser<int>> UserManager { get; }
        public IUserClaimsPrincipalFactory<IdentityUser<int>> UserClaimsPrincipalFactory { get; }
    }
}
