﻿using Conquer.Game.Messages;
using Conquer.Models;
using Microsoft.AspNetCore.Connections;
using Microsoft.Extensions.Logging;
using System;
using System.Buffers.Binary;

namespace Conquer.Game
{
    public class GameClient : ConquerClient<GameContext>
    {
        private readonly ILogger _logger;

        public GameClient(ConnectionContext connection, ILogger<GameClient> logger) : base(connection)
        {
            _logger = logger;
        }

        public Player Player { get; set; }

        protected override IMessage<GameContext> CreateMessage(ReadOnlySpan<byte> buffer)
        {
            var type = (MessageType)BinaryPrimitives.ReadUInt16LittleEndian(buffer[2..]);

            IMessage<GameContext> message = type switch
            {
                MessageType.MsgRegister => new MsgRegister(),
                MessageType.MsgTalk => new MsgTalk(),
                MessageType.MsgWalk => new MsgWalk(),
                MessageType.MsgItem => new MsgItem(),
                MessageType.MsgAction => new MsgAction(),
                MessageType.MsgConnect => new MsgConnect(),
                _ => default
            };

            if (message is null)
            {
                _logger.LogWarning("Unhandled message type {MessageType}.", type);
            }

            message?.Read(buffer);
            return message;
        }
    }
}
