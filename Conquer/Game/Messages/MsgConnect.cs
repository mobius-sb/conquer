﻿using Conquer.Cryptography;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Buffers.Binary;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conquer.Game.Messages
{
    public class MsgConnect : IMessage<GameContext>
    {
        public ushort Size => 28;
        public MessageType Type => MessageType.MsgConnect;
        public uint Id { get; set; }
        public uint Data { get; set; }
        public string Info { get; set; }

        public async Task HandleAsync(GameContext context)
        {
            // Update cipher keys
            if (context.Client.ConnectionCipher is TqConnectionCipher connectionCipher)
            {
                connectionCipher.SetKeys(Data, Id);
            }

            var serverName = await context.Cache.GetStringAsync($"User:{Id}:{Data}");

            // Key not found in cache or invalid server
            if (serverName != context.Server.Name)
            {
                await context.Client.WriteAsync(new MsgTalk
                {
                    Channel = TalkChannel.Login,
                    Strings = { MsgTalk.System, MsgTalk.AllUsers, "", "Invalid server." }
                });
                return;
            }

            var user = await context.UserManager.FindByIdAsync($"{Id}");
            context.User = await context.UserClaimsPrincipalFactory.CreateAsync(user);
            context.Player = await context.Database.Players.FirstOrDefaultAsync(p => p.UserId == user.Id);

            // New player
            if (context.Player is null)
            {
                await context.Client.WriteAsync(new MsgTalk
                {
                    Channel = TalkChannel.Login,
                    Strings = { MsgTalk.System, MsgTalk.AllUsers, "", MsgTalk.NewRole }
                });
                return;
            }

            // Already logged in
            if (context.Clients.TryGetValue(context.Player.Id, out var client))
            {
                client.Abort();
            }

            // Existing player
            context.Clients.TryAdd(context.Player.Id, context.Client);

            var spouseName = await context.Database.Players
                .Where(p => p.Id == context.Player.SpouseId)
                .Select(p => p.Name)
                .FirstOrDefaultAsync();

            await context.Client.WriteAsync(new MsgTalk
            {
                Channel = TalkChannel.Login,
                Strings = { MsgTalk.System, MsgTalk.AllUsers, "", MsgTalk.AnswerOk }
            });
            await context.Client.WriteAsync(new MsgUserInfo(context.Player, spouseName));
        }

        public void Read(ReadOnlySpan<byte> buffer)
        {
            Id = BinaryPrimitives.ReadUInt32LittleEndian(buffer[4..]);
            Data = BinaryPrimitives.ReadUInt32LittleEndian(buffer[8..]);
            Info = Encoding.Latin1.GetString(buffer.Slice(12, 16)).TrimEnd('\0');
        }

        public void Write(Span<byte> buffer)
        {
            BinaryPrimitives.WriteUInt16LittleEndian(buffer, Size);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[2..], (ushort)Type);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[4..], Id);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[8..], Data);
            Encoding.Latin1.GetBytes(Info.PadRight(16, '\0'), buffer.Slice(12, 16));
        }
    }
}
