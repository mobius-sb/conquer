﻿using System;
using System.Buffers.Binary;
using System.Threading.Tasks;

namespace Conquer.Game.Messages
{
    public enum Direction : byte
    {
        South,
        SouthWest,
        West,
        NorthWest,
        North,
        NorthEast,
        East,
        SouthEast,
    }

    public enum WalkMode : byte
    {
        Walk,
        Run
    }

    public class MsgWalk : IMessage<GameContext>
    {
        public ushort Size => 10;
        public MessageType Type => MessageType.MsgWalk;
        public uint Id { get; set; }
        public Direction Direction { get; set; }
        public WalkMode Mode { get; set; }

        public Task HandleAsync(GameContext context)
        {
            var (deltaX, deltaY) = (Direction)((byte)Direction % 8) switch
            {
                Direction.South => (0, 1),
                Direction.SouthWest => (-1, 1),
                Direction.West => (-1, 0),
                Direction.NorthWest => (-1, -1),
                Direction.North => (0, -1),
                Direction.NorthEast => (1, -1),
                Direction.East => (1, 0),
                Direction.SouthEast => (1, 1),
                _ => (0, 0)
            };

            context.Player.X = (ushort)(context.Player.X + deltaX);
            context.Player.Y = (ushort)(context.Player.Y + deltaY);

            return Task.CompletedTask;
        }

        public void Read(ReadOnlySpan<byte> buffer)
        {
            Id = BinaryPrimitives.ReadUInt32LittleEndian(buffer[4..]);
            Direction = (Direction)buffer[8];
            Mode = (WalkMode)buffer[9];
        }

        public void Write(Span<byte> buffer)
        {
            BinaryPrimitives.WriteUInt16LittleEndian(buffer, Size);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[2..], (ushort)Type);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[4..], Id);
            buffer[8] = (byte)Direction;
            buffer[9] = (byte)Mode;
        }
    }
}
