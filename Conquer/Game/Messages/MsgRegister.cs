﻿using Conquer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Buffers.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Conquer.Game.Messages
{
    public class MsgRegister : IMessage<GameContext>
    {
        public ushort Size => 60;
        public MessageType Type => MessageType.MsgRegister;
        public string UserName { get; set; }
        public string PlayerName { get; set; }
        public string Password { get; set; }
        public ushort Model { get; set; }
        public ushort Profession { get; set; }
        public uint Id { get; set; }

        public async Task HandleAsync(GameContext context)
        {
            if (Regex.IsMatch(PlayerName, @"\[|]"))
            {
                await context.Client.WriteAsync(new MsgTalk
                {
                    Channel = TalkChannel.Dialog,
                    Strings = { MsgTalk.System, MsgTalk.AllUsers, "", "Invalid name." }
                });
                return;
            }

            if (await context.Database.Players.AnyAsync(p => p.Name == PlayerName))
            {
                await context.Client.WriteAsync(new MsgTalk
                {
                    Channel = TalkChannel.Dialog,
                    Strings = { MsgTalk.System, MsgTalk.AllUsers, "", "Name taken." }
                });
                return;
            }

            var user = await context.UserManager.GetUserAsync(context.User);

            context.Player = new()
            {
                UserId = user.Id,
                Name = PlayerName,
                Model = Model,
                Hair = 310,
                PkMode = PkMode.Peace,
                Money = 1000,
                Strength = 4,
                Dexterity = 6,
                Vitality = 12,
                Mana = 10,
                Level = 1,
                Profession = (Profession)Profession,
                MapId = 1002,
                X = 438,
                Y = 377
            };
            context.Player.Health = context.Player.Strength * 3 + context.Player.Dexterity * 3 + context.Player.Mana * 3 + context.Player.Vitality * 24;
            context.Player.Magic = context.Player.Mana * 5;

            context.Database.Add(context.Player);
            await context.Database.SaveChangesAsync();

            await context.Client.WriteAsync(new MsgTalk
            {
                Channel = TalkChannel.Dialog,
                Strings = { MsgTalk.System, MsgTalk.AllUsers, "", MsgTalk.AnswerOk }
            });
        }

        public void Read(ReadOnlySpan<byte> buffer)
        {
            UserName = Encoding.Latin1.GetString(buffer.Slice(4, 16)).TrimEnd('\0');
            PlayerName = Encoding.Latin1.GetString(buffer.Slice(20, 16)).TrimEnd('\0');
            Password = Encoding.Latin1.GetString(buffer.Slice(36, 16)).TrimEnd('\0');
            Model = BinaryPrimitives.ReadUInt16LittleEndian(buffer[52..]);
            Profession = BinaryPrimitives.ReadUInt16LittleEndian(buffer[54..]);
            Id = BinaryPrimitives.ReadUInt32LittleEndian(buffer[56..]);
        }

        public void Write(Span<byte> buffer)
        {
            BinaryPrimitives.WriteUInt16LittleEndian(buffer, Size);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[2..], (ushort)Type);
            Encoding.Latin1.GetBytes(UserName.PadRight(16, '\0'), buffer.Slice(4, 16));
            Encoding.Latin1.GetBytes(PlayerName.PadRight(16, '\0'), buffer.Slice(20, 16));
            Encoding.Latin1.GetBytes(Password.PadRight(16, '\0'), buffer.Slice(36, 16));
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[52..], Model);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[54..], Profession);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[56..], Id);
        }
    }
}
