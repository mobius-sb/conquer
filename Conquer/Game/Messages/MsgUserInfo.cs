﻿using Conquer.Models;
using System;
using System.Buffers.Binary;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Conquer.Game.Messages
{
    public class MsgUserInfo : IMessage<GameContext>
    {
        public MsgUserInfo() { }

        public MsgUserInfo(Player player, string spouseName = null)
        {
            Id = (uint)player.Id;
            Model = (uint)(player.Model + player.Avatar * 10000);
            Hair = (ushort)player.Hair;
            Money = (uint)player.Money;
            Experience = player.Experience;
            Strength = (ushort)player.Strength;
            Dexterity = (ushort)player.Dexterity;
            Vitality = (ushort)player.Vitality;
            Mana = (ushort)player.Mana;
            AttributePoints = (ushort)player.AttributePoints;
            Health = (ushort)player.Health;
            Magic = (ushort)player.Magic;
            PkPoints = player.PkPoints;
            Level = player.Level;
            Profession = player.Profession;
            Rebirths = player.Rebirths;
            AutoAllot = player.Rebirths == 0;
            Strings = new() { player.Name, spouseName ?? "None" };
        }

        public ushort Size => (ushort)(62 + Strings.Sum(s => 1 + Encoding.Latin1.GetByteCount(s)));
        public MessageType Type => MessageType.MsgUserInfo;
        public uint Id { get; set; }
        public uint Model { get; set; }
        public ushort Hair { get; set; }
        public uint Money { get; set; }
        public long Experience { get; set; }
        public ushort Strength { get; set; }
        public ushort Dexterity { get; set; }
        public ushort Vitality { get; set; }
        public ushort Mana { get; set; }
        public ushort AttributePoints { get; set; }
        public ushort Health { get; set; }
        public ushort Magic { get; set; }
        public short PkPoints { get; set; }
        public byte Level { get; set; }
        public Profession Profession { get; set; }
        public bool AutoAllot { get; set; }
        public byte Rebirths { get; set; }
        public List<string> Strings { get; set; } = new();

        public void Read(ReadOnlySpan<byte> buffer)
        {
            Id = BinaryPrimitives.ReadUInt32LittleEndian(buffer[4..]);
            Model = BinaryPrimitives.ReadUInt32LittleEndian(buffer[8..]);
            Hair = BinaryPrimitives.ReadUInt16LittleEndian(buffer[12..]);
            _ = BinaryPrimitives.ReadUInt16LittleEndian(buffer[14..]);
            Money = BinaryPrimitives.ReadUInt32LittleEndian(buffer[16..]);
            Experience = BinaryPrimitives.ReadInt64LittleEndian(buffer[20..]);
            _ = BinaryPrimitives.ReadUInt64LittleEndian(buffer[28..]);
            _ = BinaryPrimitives.ReadUInt32LittleEndian(buffer[36..]);
            Strength = BinaryPrimitives.ReadUInt16LittleEndian(buffer[40..]);
            Dexterity = BinaryPrimitives.ReadUInt16LittleEndian(buffer[42..]);
            Vitality = BinaryPrimitives.ReadUInt16LittleEndian(buffer[44..]);
            Mana = BinaryPrimitives.ReadUInt16LittleEndian(buffer[46..]);
            AttributePoints = BinaryPrimitives.ReadUInt16LittleEndian(buffer[48..]);
            Health = BinaryPrimitives.ReadUInt16LittleEndian(buffer[50..]);
            Magic = BinaryPrimitives.ReadUInt16LittleEndian(buffer[52..]);
            PkPoints = BinaryPrimitives.ReadInt16LittleEndian(buffer[54..]);
            Level = buffer[56];
            Profession = (Profession)buffer[57];
            AutoAllot = buffer[58] != 0;
            Rebirths = buffer[59];
            var hasName = buffer[60] != 0;
            if (hasName)
            {
                var count = buffer[61];
                var read = 62;
                for (var i = 0; i < count; i++)
                {
                    var byteCount = buffer[read++];
                    Strings.Add(Encoding.Latin1.GetString(buffer.Slice(read, byteCount)));
                    read += byteCount;
                }
            }
        }

        public void Write(Span<byte> buffer)
        {
            BinaryPrimitives.WriteUInt16LittleEndian(buffer, Size);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[2..], (ushort)Type);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[4..], Id);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[8..], Model);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[12..], Hair);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[14..], 0);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[16..], Money);
            BinaryPrimitives.WriteInt64LittleEndian(buffer[20..], Experience);
            BinaryPrimitives.WriteUInt64LittleEndian(buffer[28..], 0);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[36..], 0);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[40..], Strength);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[42..], Dexterity);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[44..], Vitality);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[46..], Mana);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[48..], AttributePoints);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[50..], Health);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[52..], Magic);
            BinaryPrimitives.WriteInt16LittleEndian(buffer[54..], PkPoints);
            buffer[56] = Level;
            buffer[57] = (byte)Profession;
            buffer[58] = AutoAllot ? 1 : 0;
            buffer[59] = Rebirths;
            buffer[60] = Strings.Any() ? 1 : 0;
            buffer[61] = (byte)Strings.Count;
            var written = 62;
            foreach (var s in Strings)
            {
                var byteCount = (byte)Encoding.Latin1.GetByteCount(s);
                buffer[written++] = byteCount;
                Encoding.Latin1.GetBytes(s, buffer.Slice(written, byteCount));
                written += byteCount;
            }
        }
    }
}
