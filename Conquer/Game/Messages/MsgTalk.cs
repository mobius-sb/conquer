﻿using System;
using System.Buffers.Binary;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Conquer.Game.Messages
{
    public enum TalkChannel : ushort
    {
        Talk = 2000,
        Whisper = 2001,
        Dialog = 2100,
        Login = 2101
    }

    [Flags]
    public enum TalkStyles : ushort
    {
        Normal = 0b_0000,
        Scroll = 0b_0001,
        Flash = 0b_0010,
        Blast = 0b_1000
    }

    public class MsgTalk : IMessage<GameContext>
    {
        public const string
            System = "SYSTEM",
            AllUsers = "ALLUSERS",
            NewRole = "NEW_ROLE",
            AnswerOk = "ANSWER_OK";

        public ushort Size => (ushort)(17 + Strings.Sum(s => 1 + Encoding.Latin1.GetByteCount(s)));
        public MessageType Type => MessageType.MsgTalk;
        public Color Color { get; set; } = Color.White;
        public TalkChannel Channel { get; set; }
        public TalkStyles Styles { get; set; } = TalkStyles.Normal;
        public uint Id { get; set; }
        public List<string> Strings { get; set; } = new();

        public void Read(ReadOnlySpan<byte> buffer)
        {
            Color = Color.FromArgb(BinaryPrimitives.ReadInt32LittleEndian(buffer[4..]));
            Channel = (TalkChannel)BinaryPrimitives.ReadUInt16LittleEndian(buffer[8..]);
            Styles = (TalkStyles)BinaryPrimitives.ReadUInt16LittleEndian(buffer[10..]);
            Id = BinaryPrimitives.ReadUInt32LittleEndian(buffer[12..]);
            var count = buffer[16];
            var read = 17;
            for (var i = 0; i < count; i++)
            {
                var byteCount = buffer[read++];
                Strings.Add(Encoding.Latin1.GetString(buffer.Slice(read, byteCount)));
                read += byteCount;
            }
        }

        public void Write(Span<byte> buffer)
        {
            BinaryPrimitives.WriteUInt16LittleEndian(buffer, Size);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[2..], (ushort)Type);
            BinaryPrimitives.WriteInt32LittleEndian(buffer[4..], Color.FromArgb(0, Color).ToArgb());
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[8..], (ushort)Channel);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[10..], (ushort)Styles);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[12..], Id);
            buffer[16] = (byte)Strings.Count;
            var written = 17;
            foreach (var s in Strings)
            {
                var byteCount = (byte)Encoding.Latin1.GetByteCount(s);
                buffer[written++] = byteCount;
                Encoding.Latin1.GetBytes(s, buffer.Slice(written, byteCount));
                written += byteCount;
            }
        }
    }
}
