﻿using Microsoft.Extensions.Logging;
using System;
using System.Buffers.Binary;
using System.Threading.Tasks;

namespace Conquer.Game.Messages
{
    public enum ItemActionType : uint
    {
        ShopPurchase = 1,
        ShopSell,
        InventoryRemove,
        InventoryEquip,
        EquipmentWear,
        EquipmentRemove,
        EquipmentSplit,
        EquipmentCombine,
        BankQuery,
        BankDeposit,
        BankWithdraw,
        InventoryDropSilver,
        EquipmentRepair = 14,
        EquipmentRepairAll,
        EquipmentImprove = 19,
        EquipmentLevelUp,
        BoothQuery,
        BoothSell,
        BoothRemove,
        BoothPurchase,
        EquipmentAmount,
        ClientPing = 27,
        EquipmentEnchant,
        BoothSellPoints
    }

    public class MsgItem : IMessage<GameContext>
    {
        private readonly Memory<byte> _data = new byte[4];

        public ushort Size => 20;
        public MessageType Type => MessageType.MsgItem;
        public uint Id { get; set; }
        public uint Data
        {
            get => BinaryPrimitives.ReadUInt32LittleEndian(_data.Span);
            set => BinaryPrimitives.WriteUInt32LittleEndian(_data.Span, value);
        }
        public ushort X
        {
            get => BinaryPrimitives.ReadUInt16LittleEndian(_data.Span);
            set => BinaryPrimitives.WriteUInt16LittleEndian(_data.Span, value);
        }
        public ushort Y
        {
            get => BinaryPrimitives.ReadUInt16LittleEndian(_data[2..].Span);
            set => BinaryPrimitives.WriteUInt16LittleEndian(_data[2..].Span, value);
        }
        public ItemActionType ItemActionType { get; set; }
        public uint Timestamp { get; set; } = (uint)Environment.TickCount;

        public Task HandleAsync(GameContext context)
        {
            return ItemActionType switch
            {
                ItemActionType.ClientPing => ClientPing(),
                _ => Unhandled()
            };

            async Task ClientPing()
            {
                context.Database.Update(context.Player);
                await context.Database.SaveChangesAsync();
                await context.Client.WriteAsync(this);
            }

            async Task Unhandled()
            {
                context.Logger.LogWarning("Unhandled item action type {ItemActionType}.", ItemActionType);
                await context.Client.WriteAsync(this);
            }
        }

        public void Read(ReadOnlySpan<byte> buffer)
        {
            Id = BinaryPrimitives.ReadUInt32LittleEndian(buffer[4..]);
            buffer[8..12].CopyTo(_data.Span);
            ItemActionType = (ItemActionType)BinaryPrimitives.ReadUInt32LittleEndian(buffer[12..]);
            Timestamp = BinaryPrimitives.ReadUInt32LittleEndian(buffer[16..]);
        }

        public void Write(Span<byte> buffer)
        {
            BinaryPrimitives.WriteUInt16LittleEndian(buffer, Size);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[2..], (ushort)Type);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[4..], Id);
            _data.Span.CopyTo(buffer[8..]);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[12..], (uint)ItemActionType);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[16..], Timestamp);
        }
    }
}
