﻿using Microsoft.Extensions.Logging;
using System;
using System.Buffers.Binary;
using System.Threading.Tasks;

namespace Conquer.Game.Messages
{
    public enum ActionType
    {
        CharacterDirection = 124,
        CharacterEmote = 126,
        MapPortal = 130,
        MapTeleport = 131,
        LoginSpawn = 137,
        LoginInventory,
        LoginRelationships,
        MapRemoveSpawn = 141,
        MapJump,
        SpellRemove = 144,
        ProficiencyRemove,
        CharacterLevelUp,
        SpellAbortXp,
        CharacterRevive,
        CharacterDelete,
        LoginProficiencies,
        LoginSpells,
        CharacterPkMode,
        LoginGuild,
        CharacterDead,
        RelationshipsFriend = 156,
        MapMine = 159,
        MapEffect = 162,
        MapKickBack = 164,
        BoothSpawn = 167,
        BoothSuspend,
        BoothResume,
        BoothLeave,
        ClientCommand = 172,
        SpellAbortTransform = 174,
        CharacterObservation,
        SpellAbortFlight,
        MapGold,
        ClientDialog = 186,
        LoginComplete = 190,
        MapQuery = 232
    }

    public class MsgAction : IMessage<GameContext>
    {
        private readonly Memory<byte> _data = new byte[8];

        public ushort Size => 28;
        public MessageType Type => MessageType.MsgAction;
        public uint Timestamp { get; set; } = (uint)Environment.TickCount;
        public uint Id { get; set; }
        public ushort X { get; set; }
        public ushort Y { get; set; }
        public uint DataUInt1
        {
            get => BinaryPrimitives.ReadUInt32LittleEndian(_data.Span);
            set => BinaryPrimitives.WriteUInt32LittleEndian(_data.Span, value);
        }
        public ushort DataUShort1
        {
            get => BinaryPrimitives.ReadUInt16LittleEndian(_data.Span);
            set => BinaryPrimitives.WriteUInt16LittleEndian(_data.Span, value);
        }
        public ushort DataUShort2
        {
            get => BinaryPrimitives.ReadUInt16LittleEndian(_data[2..].Span);
            set => BinaryPrimitives.WriteUInt16LittleEndian(_data[2..].Span, value);
        }
        public uint DataUInt2
        {
            get => BinaryPrimitives.ReadUInt32LittleEndian(_data[4..].Span);
            set => BinaryPrimitives.WriteUInt32LittleEndian(_data[4..].Span, value);
        }
        public ushort DataUShort3
        {
            get => BinaryPrimitives.ReadUInt16LittleEndian(_data[4..].Span);
            set => BinaryPrimitives.WriteUInt16LittleEndian(_data[4..].Span, value);
        }
        public ushort DataUShort4
        {
            get => BinaryPrimitives.ReadUInt16LittleEndian(_data[6..].Span);
            set => BinaryPrimitives.WriteUInt16LittleEndian(_data[6..].Span, value);
        }
        public ActionType ActionType { get; set; }

        public Task HandleAsync(GameContext context)
        {
            return ActionType switch
            {
                ActionType.LoginSpawn => LoginSpawn(),
                ActionType.MapJump => MapJump(),
                ActionType.LoginGuild => LoginGuild(),
                _ => Unhandled()
            };

            async Task LoginSpawn()
            {
                Id = (uint)context.Player.Id;
                X = (ushort)context.Player.X;
                Y = (ushort)context.Player.Y;
                DataUInt2 = (uint)context.Player.MapId;
                await context.Client.WriteAsync(this);
            }

            Task MapJump()
            {
                context.Player.X = DataUShort3;
                context.Player.Y = DataUShort4;
                return Task.CompletedTask;
            }

            async Task LoginGuild()
            {
                await context.Client.WriteAsync(new MsgTalk
                {
                    Channel = TalkChannel.Talk,
                    Strings = { MsgTalk.System, "", "", $"Welcome to {context.Server.Name}!" }
                });
                await context.Client.WriteAsync(this);
            }

            async Task Unhandled()
            {
                context.Logger.LogWarning("Unhandle action type {ActionType}.", ActionType);
                await context.Client.WriteAsync(this);
            }
        }

        public void Read(ReadOnlySpan<byte> buffer)
        {
            Timestamp = BinaryPrimitives.ReadUInt32LittleEndian(buffer[4..]);
            Id = BinaryPrimitives.ReadUInt32LittleEndian(buffer[8..]);
            X = BinaryPrimitives.ReadUInt16LittleEndian(buffer[12..]);
            Y = BinaryPrimitives.ReadUInt16LittleEndian(buffer[14..]);
            buffer[16..24].CopyTo(_data.Span);
            ActionType = (ActionType)BinaryPrimitives.ReadUInt32LittleEndian(buffer[24..]);
        }

        public void Write(Span<byte> buffer)
        {
            BinaryPrimitives.WriteUInt16LittleEndian(buffer, Size);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[2..], (ushort)Type);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[4..], Timestamp);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[8..], Id);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[12..], X);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[14..], Y);
            _data.Span.CopyTo(buffer[16..]);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[24..], (ushort)ActionType);
        }
    }
}
