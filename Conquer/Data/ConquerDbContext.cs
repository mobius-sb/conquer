﻿using Conquer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Conquer.Data
{
    public class ConquerDbContext : IdentityDbContext<IdentityUser<int>, IdentityRole<int>, int>
    {
        public ConquerDbContext(DbContextOptions<ConquerDbContext> options)
            : base(options)
        {
        }

        public DbSet<Ban> Bans { get; set; }
        public DbSet<Server> Servers { get; set; }
        public DbSet<Player> Players { get; set; }
    }
}
