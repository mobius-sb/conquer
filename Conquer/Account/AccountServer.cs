﻿using Microsoft.AspNetCore.Connections;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Conquer.Account
{
    public class AccountServer : ConnectionHandler
    {
        private readonly AccountServerOptions _options;
        private readonly IServiceProvider _serviceProvider;
        private readonly ILoggerFactory _loggerFactory;
        private readonly ILogger _logger;

        public AccountServer(IOptions<AccountServerOptions> options, IServiceProvider serviceProvider, ILoggerFactory loggerFactory)
        {
            _options = options.Value;
            _serviceProvider = serviceProvider;
            _loggerFactory = loggerFactory;
            _logger = loggerFactory.CreateLogger<AccountServer>();
        }

        public override async Task OnConnectedAsync(ConnectionContext connection)
        {
            var client = ActivatorUtilities.CreateInstance<AccountClient>(_serviceProvider, connection);

            _logger.LogInformation("{RemoteEndPoint} connected.", client.RemoteEndPoint);

            try
            {
                using var source = new CancellationTokenSource();
                while (true)
                {
                    source.CancelAfter(_options.ClientTimeoutInterval);
                    var message = await client.ReadAsync(source.Token);

                    if (message is null)
                    {
                        break;
                    }

                    using var scope = _serviceProvider.CreateScope();
                    var context = ActivatorUtilities.CreateInstance<AccountContext>(scope.ServiceProvider, client, _loggerFactory.CreateLogger(message.GetType()));
                    await message.HandleAsync(context);
                }
            }
            catch (OperationCanceledException)
            {
                // Don't let operation canceled exceptions out
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unexpected exception in {Method}.", nameof(OnConnectedAsync));
            }

            _logger.LogInformation("{RemoteEndPoint} disconnected.", client.RemoteEndPoint);
        }
    }
}
