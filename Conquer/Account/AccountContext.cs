﻿using Conquer.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using System;
using System.Security.Claims;
using System.Security.Cryptography;

namespace Conquer.Account
{
    public class AccountContext
    {
        public AccountContext(
            ConquerDbContext database,
            AccountServer server,
            AccountClient client,
            IServiceProvider serviceProvider,
            ILogger logger,
            RandomNumberGenerator randomNumberGenerator,
            IDistributedCache cache,
            UserManager<IdentityUser<int>> userManager,
            IUserClaimsPrincipalFactory<IdentityUser<int>> userClaimsPrincipalFactory)
        {
            Database = database;
            Server = server;
            Client = client;
            ServiceProvider = serviceProvider;
            Logger = logger;
            RandomNumberGenerator = randomNumberGenerator;
            Cache = cache;
            UserManager = userManager;
            UserClaimsPrincipalFactory = userClaimsPrincipalFactory;
        }

        public ConquerDbContext Database { get; }
        public AccountServer Server { get; }
        public AccountClient Client { get; }
        public ClaimsPrincipal User { get => Client.User; set => Client.User = value; }
        public IServiceProvider ServiceProvider { get; }
        public ILogger Logger { get; }
        public RandomNumberGenerator RandomNumberGenerator { get;}
        public IDistributedCache Cache { get; }
        public UserManager<IdentityUser<int>> UserManager { get; }
        public IUserClaimsPrincipalFactory<IdentityUser<int>> UserClaimsPrincipalFactory { get; }
    }
}
