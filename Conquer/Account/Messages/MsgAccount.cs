﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Buffers.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Conquer.Account.Messages
{
    public class MsgAccount : IMessage<AccountContext>
    {
        public ushort Size => 52;
        public MessageType Type => MessageType.MsgAccount;
        public string UserName { get; set; }
        public byte[] EncryptedPassword { get; set; }
        public string Server { get; set; }

        public async Task HandleAsync(AccountContext context)
        {
            var password = DecryptPassword();

            var user = await context.UserManager.FindByNameAsync(UserName);

            // User doesn't exist
            if(user is null)
            {
                await context.Client.WriteAsync(new MsgConnectEx(RejectionCode.InvalidAccount));
                return;
            }

            // Locked out
            if (await context.UserManager.IsLockedOutAsync(user))
            {
                await context.Client.WriteAsync(new MsgConnectEx(RejectionCode.TryAgainLater));
                return;
            }

            // Invalid password
            if (!await context.UserManager.CheckPasswordAsync(user, password))
            {
                await context.UserManager.AccessFailedAsync(user);
                await context.Client.WriteAsync(new MsgConnectEx(RejectionCode.InvalidAccount));
                return;
            }

            var banned = await context.Database.Bans.AnyAsync(b => b.UserId == user.Id && (!b.Expires.HasValue || b.Expires > DateTime.Now));

            // Banned
            if (banned)
            {
                await context.Client.WriteAsync(new MsgConnectEx(RejectionCode.Banned));
                return;
            }

            var server = await context.Database.Servers.FirstOrDefaultAsync(s => s.Name == Server);

            // Invalid server
            if (server is null)
            {
                await context.Client.WriteAsync(new MsgConnectEx(RejectionCode.ServerDown));
                return;
            }

            // Success
            var token = GenerateToken();
            await context.Cache.SetStringAsync($"User:{user.Id}:{token}", Server, new DistributedCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(30)
            });

            context.User = await context.UserClaimsPrincipalFactory.CreateAsync(user);

            await context.Client.WriteAsync(new MsgConnectEx((uint)user.Id, token, server.IpAddress, (ushort)server.Port));

            string DecryptPassword()
            {
                Span<byte> decryptedPassword = stackalloc byte[EncryptedPassword.Length];
                context.Client.PasswordCipher.Decrypt(EncryptedPassword, decryptedPassword);
                var index = decryptedPassword.IndexOf((byte)0);
                return Encoding.Latin1.GetString(decryptedPassword[..index]);
            }

            uint GenerateToken()
            {
                Span<byte> buffer = stackalloc byte[sizeof(uint)];
                context.RandomNumberGenerator.GetBytes(buffer);
                return BinaryPrimitives.ReadUInt32LittleEndian(buffer);
            }
        }

        public void Read(ReadOnlySpan<byte> buffer)
        {
            UserName = Encoding.Latin1.GetString(buffer.Slice(4, 16)).TrimEnd('\0');
            EncryptedPassword = buffer.Slice(20, 16).ToArray();
            Server = Encoding.Latin1.GetString(buffer.Slice(36, 16)).TrimEnd('\0');
        }

        public void Write(Span<byte> buffer)
        {
            BinaryPrimitives.WriteUInt16LittleEndian(buffer, Size);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[2..], (ushort)Type);
            Encoding.Latin1.GetBytes(UserName.PadRight(16, '\0'), buffer.Slice(4, 16));
            EncryptedPassword.CopyTo(buffer.Slice(20, 16));
            Encoding.Latin1.GetBytes(UserName.PadRight(16, '\0'), buffer.Slice(36, 16));
        }
    }
}
