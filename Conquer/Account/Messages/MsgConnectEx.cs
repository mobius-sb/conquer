﻿using System;
using System.Buffers.Binary;
using System.Text;

namespace Conquer.Account.Messages
{
    public enum RejectionCode : uint
    {
        InvalidAccount = 1,
        ServerDown = 10,
        TryAgainLater = 11,
        Banned = 12,
        UnknownError = 999
    }

    public class MsgConnectEx : IMessage<AccountContext>
    {
        public MsgConnectEx() { }

        public MsgConnectEx(uint id, uint token, string ipAddress, ushort port) =>
            (Id, Data, Info, Port) = (id, token, ipAddress, port);

        public MsgConnectEx(RejectionCode code)
        {
            Data = (uint)code;
            Info = code switch
            {
                RejectionCode.InvalidAccount => "ÕÊºÅÃû»ò¿ÚÁî´í",
                RejectionCode.ServerDown => "·þÎñÆ÷Î´Æô¶¯",
                RejectionCode.TryAgainLater => "ÇëÉÔºóÖØÐÂµÇÂ¼",
                RejectionCode.Banned => "¸ÃÕÊºÅ±»·âºÅ",
                _ => "Êý¾Ý¿â´íÎó"
            };
        }

        public ushort Size => 30;
        public MessageType Type => MessageType.MsgConnectEx;
        public uint Id { get; set; }
        public uint Data { get; set; }
        public string Info { get; set; }
        public ushort Port { get; set; }

        public void Read(ReadOnlySpan<byte> buffer)
        {
            Id = BinaryPrimitives.ReadUInt32LittleEndian(buffer[4..]);
            Data = BinaryPrimitives.ReadUInt32LittleEndian(buffer[8..]);
            Info = Encoding.Latin1.GetString(buffer.Slice(12, 16)).TrimEnd('\0');
            Port = BinaryPrimitives.ReadUInt16LittleEndian(buffer[28..]);
        }

        public void Write(Span<byte> buffer)
        {
            BinaryPrimitives.WriteUInt16LittleEndian(buffer, Size);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[2..], (ushort)Type);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[4..], Id);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[8..], Data);
            Encoding.Latin1.GetBytes(Info.PadRight(16, '\0'), buffer.Slice(12, 16));
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[28..], Port);
        }
    }
}
