﻿using System;
using System.Buffers.Binary;
using System.Text;

namespace Conquer.Account.Messages
{
    public class MsgConnect : IMessage<AccountContext>
    {
        public ushort Size => 28;
        public MessageType Type => MessageType.MsgConnect;
        public uint Id { get; set; }
        public uint Data { get; set; }
        public string Info { get; set; }

        public void Read(ReadOnlySpan<byte> buffer)
        {
            Id = BinaryPrimitives.ReadUInt32LittleEndian(buffer[4..]);
            Data = BinaryPrimitives.ReadUInt32LittleEndian(buffer[8..]);
            Info = Encoding.Latin1.GetString(buffer.Slice(12, 16)).TrimEnd('\0');
        }

        public void Write(Span<byte> buffer)
        {
            BinaryPrimitives.WriteUInt16LittleEndian(buffer, Size);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer[2..], (ushort)Type);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[4..], Id);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer[8..], Data);
            Encoding.Latin1.GetBytes(Info.PadRight(16, '\0'), buffer.Slice(12, 16));
        }
    }
}
