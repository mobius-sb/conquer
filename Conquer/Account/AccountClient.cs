﻿using Conquer.Account.Messages;
using Conquer.Cryptography;
using Microsoft.AspNetCore.Connections;
using Microsoft.Extensions.Logging;
using System;
using System.Buffers.Binary;

namespace Conquer.Account
{
    public class AccountClient : ConquerClient<AccountContext>
    {
        private readonly ILogger _logger;

        public AccountClient(ConnectionContext connection, IPasswordCipher passwordCipher, ILogger<AccountClient> logger)
            : base(connection)
        {
            PasswordCipher = passwordCipher;
            _logger = logger;
        }

        public IPasswordCipher PasswordCipher { get; init; }

        protected override IMessage<AccountContext> CreateMessage(ReadOnlySpan<byte> buffer)
        {
            var type = (MessageType)BinaryPrimitives.ReadUInt16LittleEndian(buffer[2..]);

            IMessage<AccountContext> message = type switch
            {
                MessageType.MsgAccount => new MsgAccount(),
                MessageType.MsgConnect => new MsgConnect(),
                _ => default
            };

            if (message is null)
            {
                _logger.LogWarning("Unhandled message type {MessageType}.", type);
            }

            message?.Read(buffer);
            return message;
        }
    }
}
