﻿using System;

namespace Conquer.Account
{
    public class AccountServerOptions
    {
        public int Port { get; set; }
        public TimeSpan ClientTimeoutInterval { get; set; }
    }
}
