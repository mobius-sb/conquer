﻿using Conquer.Cryptography;
using Microsoft.AspNetCore.Connections;
using System;
using System.Buffers;
using System.Buffers.Binary;
using System.IO;
using System.Net;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Conquer
{
    public abstract class ConquerClient<T>
    {
        private const int MaximumMessageSize = 1024;

        private readonly ConnectionContext _connection;
        private readonly SemaphoreSlim _writeSemaphore = new(1);

        public ConquerClient(ConnectionContext connection)
        {
            _connection = connection;
        }

        public EndPoint RemoteEndPoint => _connection.RemoteEndPoint;
        public IConnectionCipher ConnectionCipher => _connection.Features.Get<IConnectionCipher>();
        public ClaimsPrincipal User { get; set; }

        public async ValueTask<IMessage<T>> ReadAsync(CancellationToken cancellationToken = default)
        {
            while (true)
            {
                var result = await _connection.Transport.Input.ReadAsync(cancellationToken);
                var buffer = result.Buffer;

                SequencePosition consumed = buffer.Start;
                SequencePosition examined = buffer.End;

                try
                {
                    if (TryParseMessage(result.Buffer, ref consumed, ref examined, out var message))
                    {
                        return message;
                    }
                }
                finally
                {
                    _connection.Transport.Input.AdvanceTo(consumed, examined);
                }

                if (result.IsCompleted)
                {
                    break;
                }
            }

            return default;
        }

        private bool TryParseMessage(in ReadOnlySequence<byte> input, ref SequencePosition consumed, ref SequencePosition examined, out IMessage<T> message)
        {
            if (input.Length < sizeof(ushort))
            {
                examined = input.End;
                message = default;
                return false;
            }

            var sizeSlice = input.Slice(0, sizeof(ushort));
            ushort size;

            if (sizeSlice.IsSingleSegment)
            {
                size = BinaryPrimitives.ReadUInt16LittleEndian(sizeSlice.FirstSpan);
            }
            else
            {
                Span<byte> buffer = stackalloc byte[sizeof(ushort)];
                sizeSlice.CopyTo(buffer);
                size = BinaryPrimitives.ReadUInt16LittleEndian(buffer);
            }

            if (size > MaximumMessageSize)
            {
                throw new InvalidDataException($"The maximum message size of {MaximumMessageSize}B was exceeded");
            }

            if (input.Length < size)
            {
                examined = input.End;
                message = default;
                return false;
            }

            consumed = input.GetPosition(size);
            examined = consumed;

            var messageSlice = input.Slice(0, size);
            
            if (messageSlice.IsSingleSegment)
            {
                message = CreateMessage(messageSlice.FirstSpan);
            }
            else
            {
                Span<byte> buffer = stackalloc byte[size];
                messageSlice.CopyTo(buffer);
                message = CreateMessage(buffer);
            }

            return message != default;
        }

        protected abstract IMessage<T> CreateMessage(ReadOnlySpan<byte> buffer);

        public async ValueTask<bool> WriteAsync(IMessage<T> message, CancellationToken cancellationToken = default)
        {
            await _writeSemaphore.WaitAsync(cancellationToken);

            try
            {
                WriteMessage();
                var result = await _connection.Transport.Output.FlushAsync();
                return !(result.IsCanceled || result.IsCompleted);
            }
            finally
            {
                _writeSemaphore.Release();
            }

            void WriteMessage()
            {
                var size = message.Size;
                var buffer = _connection.Transport.Output.GetSpan(size);
                message.Write(buffer.Slice(0, size));
                _connection.Transport.Output.Advance(size);
            }
        }

        public void Abort() => _connection.Abort();
    }
}
