﻿using Conquer.Account;
using Conquer.Cryptography;
using Conquer.Data;
using Conquer.Game;
using Conquer.Middleware;
using Microsoft.AspNetCore.Connections;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System.Security.Cryptography;

namespace Conquer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((context, services) =>
                {
                    services.AddDbContextPool<ConquerDbContext>(options =>
                        options.UseSqlServer(context.Configuration.GetConnectionString("DefaultConnection")));

                    services.AddIdentityCore<IdentityUser<int>>(options => options.Password.RequireNonAlphanumeric = false)
                        .AddEntityFrameworkStores<ConquerDbContext>();

                    services.AddDistributedMemoryCache();

                    services.AddTransient<IPasswordCipher, Rc5PasswordCipher>();

                    services.AddSingleton<RandomNumberGenerator, RNGCryptoServiceProvider>();

                    services.AddSingleton<AccountServer>();
                    services.Configure<AccountServerOptions>(context.Configuration.GetSection(nameof(AccountServer)));

                    services.AddSingleton<GameServer>();
                    services.Configure<GameServerOptions>(context.Configuration.GetSection(nameof(GameServer)));
                })
                .ConfigureWebHostDefaults(webHost =>
                {
                    webHost.ConfigureKestrel((context, server) =>
                    {
                        var accountServerOptions = server.ApplicationServices.GetRequiredService<IOptions<AccountServerOptions>>().Value;
                        server.ListenAnyIP(accountServerOptions.Port, listen =>
                            listen.UseConnectionCipher<TqConnectionCipher>()
                                  .UseConnectionLogging(nameof(AccountServer))
                                  .UseConnectionHandler<AccountServer>());

                        var gameServerOptions = server.ApplicationServices.GetRequiredService<IOptions<GameServerOptions>>().Value;
                        server.ListenAnyIP(gameServerOptions.Port, listen =>
                            listen.UseConnectionCipher<TqConnectionCipher>()
                                  .UseConnectionLogging(nameof(GameServer))
                                  .UseConnectionHandler<GameServer>());
                    });

                    webHost.Configure(_ => { });
                });
    }
}
